package kr.pe.lahuman.sql;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.lang.management.ManagementFactory;

import kr.pe.lahuman.sql.thread.SqlFileRunner;

public class ExecuteSQL {

    public static void main(String[] args) throws InterruptedException, ClassNotFoundException, IOException {
    	
    	
        if(args != null && args.length > 0){
            File file = new File(args[0]);
            if(file.isFile() && file.exists()){
            	//single mode
                new SqlFileRunner(file).start();
            }
        }else{
        	//thread mode
        	
        	String pid = ManagementFactory.getRuntimeMXBean().getName();
        	File pidFile = new File(Constants.getInstance().getProperty("input-path") + File.separator + pid);
        	pidFile.createNewFile();
        	
        	final File inputDir = new File(Constants.getInstance().getProperty("input-path"));
        	
        	if(inputDir.isDirectory()){
        		while (pidFile.exists()) {
        			File[] sqlFiles = inputDir.listFiles(new FilenameFilter() {
        				
        				@Override
        				public boolean accept(File dir, String name) {
        					return name.toLowerCase().endsWith(".sql");
        				}
        			});
        			
        			for(File sqlFile : sqlFiles){
        				new SqlFileRunner(sqlFile).start();
        			}
        			
        			//5초에 한번
        			Thread.sleep(1000*5);
        		}
        	}
        }
    }



}
