package kr.pe.lahuman.sql;

import java.io.IOException;
import java.util.Properties;


public class Constants {

	private Properties pros = new Properties();
	private static Constants instance= new Constants();
	private Constants() {
		try {
			loadProperty();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}		
	};
	
	private void loadProperty() throws IOException {
		pros.load(getClass().getResourceAsStream("/resource/config.properties"));
		
	}

	public static Constants getInstance() {
		if(instance == null){
			instance= new Constants();
		}
		return instance;
	}
	
	public String getProperty(String key){
		return pros.getProperty(key);
	}
	
}
