package kr.pe.lahuman.sql.thread;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import kr.pe.lahuman.sql.db.RunSQL;
import kr.pe.lahuman.sql.file.FileUtils;

public class SqlFileRunner extends Thread {

	private File sqlFile = null;
	public SqlFileRunner(File file){
		sqlFile = file;
	}
	
	@Override
	public void run() {
		try {
            //Read File
            FileUtils fu = new FileUtils(sqlFile);
            String sql = fu.read();
            //Oracle
            List<Map<String, String>> dbData =   new RunSQL().run(sql);

            //Write File
            fu.write(dbData);

            //remove File
            fu.remove();
        } catch (IOException e) {
            e.printStackTrace();
        } 

	}

}
