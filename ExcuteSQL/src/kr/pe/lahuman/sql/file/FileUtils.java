package kr.pe.lahuman.sql.file;

import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import kr.pe.lahuman.sql.Constants;

public class FileUtils {
	private File file = null;
	private String sql = "";
	public FileUtils(File file) {
		this.file = file;
	}

	public String getFileName(){
		String fileSplit[] = file.getName().split("\\.");
		return fileSplit[0];
	}
	public String read() throws IOException {
		BufferedReader in = null;
		String resultSQL = "";
		try {
//			in = new BufferedReader(new FileReader(file));
			in = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF-8"));
			String s = null;
			while ((s = in.readLine()) != null) {
				resultSQL += s +"\r\n" ;
			}
		} finally {
			if (in != null) {
				in.close();
			}
		}
		//이상한 문자가 들어가 있어서 오류 발생함.
		resultSQL = resultSQL.replaceAll("﻿", "");
		
		sql = resultSQL;
		//file.renameTo 를 이용할 경우, 특이하게 root 권한으로 생성되어 삭제 불가.
		file.renameTo(new File(Constants.getInstance().getProperty("input-path")+File.separator+getFileName()+".lock"));
//		file.delete();
//		file = new File(Constants.getInstance().getProperty("input-path")+File.separator+getFileName()+".lock");
//		file.createNewFile();
		return resultSQL;
	}

	public File write(List<Map<String, String>> dbData) throws IOException {
		File outputFile = new File(Constants.getInstance().getProperty("ouput-path")
				+ File.separator + getFileName() + ".out");
		BufferedWriter out = null;
		try {
			out = new BufferedWriter(new FileWriter(outputFile));
			writeNline(out, "#SQL [");

			writeNline(out, sql);

			writeNline(out, "]");
			
			writeNline(out, "#결과 [");
			
			List<String> columnList = new ArrayList<String>();
			for(int i=0; i<dbData.size(); i++){
				Map<String, String> data = dbData.get(i);
				if(i==0){
					//column 나열
					Iterator<String> iterator = data.keySet().iterator();
					String column = "";
					while(iterator.hasNext()){
						String key = iterator.next();
						columnList.add(key);
						column += key+"|";
					}
					writeNline(out, column );		
				}
				String dataString = "";
				for(String key : columnList){
					dataString += data.get(key)+"|";
				}
				
				writeNline(out, dataString);		
			}
			
			
			writeNline(out, "]");
		} finally {
			if (out != null) {
				out.close();
			}
		}

		return outputFile;
	}

	private void writeNline(BufferedWriter out, String string)
			throws IOException {
		out.write(string);
		out.newLine();
	}

	public void remove() {
		if (file.exists()) {
			file.delete();
		}
	}
}
