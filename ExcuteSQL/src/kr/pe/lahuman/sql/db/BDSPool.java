package kr.pe.lahuman.sql.db;

import kr.pe.lahuman.sql.Constants;

import org.apache.commons.dbcp.BasicDataSource;

public class BDSPool {

	private final String url = Constants.getInstance().getProperty("db-url");
	private final String user = Constants.getInstance().getProperty("db-user");
	private final String pass = Constants.getInstance().getProperty("db-passwd");

	    
	private BasicDataSource ds = new BasicDataSource();
	public BasicDataSource getDS(){
		return ds;
	}
	private BDSPool(){
		 ds.setDriverClassName("oracle.jdbc.driver.OracleDriver");
		 ds.setUsername(user);
		 ds.setPassword(pass);
		 ds.setUrl(url);
		 ds.setValidationQuery("select 1 from dual");
		 ds.setTestOnBorrow(true);
	}
	
	public static BDSPool getInstance(){
		return new BDSPool();
	}
	
}
