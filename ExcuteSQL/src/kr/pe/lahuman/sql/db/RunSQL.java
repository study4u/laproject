package kr.pe.lahuman.sql.db;


import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import kr.pe.lahuman.sql.Constants;

public class RunSQL {

   
    public List<Map<String, String>> run(String sql){
        Connection con = null;
        List<Map<String, String>> dbData = new ArrayList<Map<String, String>>();
        try {
            con = BDSPool.getInstance().getDS().getConnection();
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            //make META
            ResultSetMetaData metaData = rs.getMetaData();

            int count = metaData.getColumnCount();
            while(rs.next()){
                Map<String, String> data = new TreeMap<String, String>();

                for (int i = 1; i <= count; i++)
                {
                    data.put(metaData.getColumnName(i), rs.getString(metaData.getColumnName(i)));
                }
                dbData.add(data);
            }

        } catch (SQLException e) {
        	e.printStackTrace();
        	Map<String, String> errorData = new HashMap<String, String>();
        	errorData.put("ERROR 발생", "["+e.getSQLState()+"]"+e.getMessage());
        	dbData.add(errorData);
        }finally {
            if(con != null){
                try {
                    con.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

        return dbData;
    }
}

