#!/bin/sh
JAVA_HOME=/usr/lib/jvm/java-1.6.0-openjdk-1.6.0.0.x86_64

export LANG=ko_KR.EUC-KR
export LC_ALL=ko_KR.EUC-KR

if [ -z "$JAVA_HOME" ] ; then
	echo "You must set JAVA_HOME to point at your JDK installation"
	exit
fi
if [ ! -f "$JAVA_HOME/bin/java" ] ; then
	echo "Cannot find java executable file in $JAVA_HOME/bin"
	echo "Please check your JAVA_HOME setting"
fi
#jAVA HOME SETTING
JAVA_LIBPATH=${JAVA_HOME}/lib:${JAVA_HOME}/jre/lib
ROOT_PATH=/data/app/excuteSQL

echo ${JAVA_LIBPATH}
export JAVA_LIBPATH

CLASSPATH=.
CLASSPATH=$CLASSPATH:$JAVA_LIBPATH
CLASSPATH=$CLASSPATH:${ROOT_PATH}/lib/ojdbc5.jar

echo $CLASSPATH

${JAVA_HOME}/bin/java -classpath $CLASSPATH kr.pe.lahuman.sql.ExecuteSQL 